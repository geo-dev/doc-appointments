using Appointments.CORE.DTOs;
using Appointments.CORE.Models;

namespace Appointments.CORE.Interfaces
{
    public interface ISlotService
    {
        Task<WeeklySlot?> GetWeeklySlotsAsync(string week);
        Task<bool> BookAppointmentAsync(AppointmentDTO appointment);
    }
}