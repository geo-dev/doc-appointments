using Appointments.CORE.DTOs;

namespace Appointments.CORE.Interfaces
{
    public interface IAppointmentsService
    {
        Task<AvailableSlotsDTO?> GetWeeklySlotsAsync(DateTime week);
        Task<bool> BookAppointmentAsync(AppointmentDTO appointment);
    }
}