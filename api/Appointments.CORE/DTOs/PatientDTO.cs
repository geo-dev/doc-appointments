using System.ComponentModel.DataAnnotations;
using Appointments.CORE.Validations;

namespace Appointments.CORE.DTOs
{
    public class PatientDTO
    {
        [Required]
        public string Name { get; set; }   
        public string? SecondName { get; set; }   
        [ValidEmail]
        public string Email { get; set; }   
        [ValidPhone]
        public string Phone { get; set; }   
    }
}