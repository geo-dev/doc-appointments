using System.ComponentModel.DataAnnotations;
using Appointments.CORE.Validations;

namespace Appointments.CORE.DTOs
{
    public class AppointmentDTO
    {
        [Required]
        public string FacilityId { get; set; }
        [ValidDateRange("End")]
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string? Comments { get; set; }
        [Required]
        public PatientDTO Patient { get; set; }
    }
}