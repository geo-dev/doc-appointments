namespace Appointments.CORE.DTOs
{
    public class AvailableSlotsDTO
    {
        public string FacilityId { get; set; }
        public DayDTO? Monday { get; set; }
        public DayDTO? Tuesday { get; set; }
        public DayDTO? Wednesday { get; set; }
        public DayDTO? Thursday { get; set; }
        public DayDTO? Friday { get; set; }
    }

    public class DayDTO
    {
        public string Date { get; set; }
        public List<SlotDTO> Slots { get; set; }
    }

    public class SlotDTO
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}