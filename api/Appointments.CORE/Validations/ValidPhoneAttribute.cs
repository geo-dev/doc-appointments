using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Appointments.CORE.Validations
{
    public class ValidPhoneAttribute : ValidationAttribute
    {
         public override bool IsValid(object? value)
        {
            if (value == null || string.IsNullOrWhiteSpace((string)value))
                return false;
                
            // It can only contain digits or spaces
            string pattern = @"^[0-9\s]+$";
            return Regex.IsMatch((string)value, pattern);
        }

    }
}