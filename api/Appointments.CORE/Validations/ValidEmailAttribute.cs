using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Appointments.CORE.Validations
{
    public class ValidEmailAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            if (value == null || string.IsNullOrWhiteSpace((string)value))
                return false;
                
            string emailPattern = @"^[^\s@]+@[^\s@]+\.[^\s@]+$";
            return Regex.IsMatch((string)value, emailPattern);      
        }
    }
}