using System.ComponentModel.DataAnnotations;

namespace Appointments.CORE.Validations
{
    public class ValidDateRangeAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;

        public ValidDateRangeAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var startDate = (DateTime)value;
            var property = validationContext.ObjectType.GetProperty(_comparisonProperty);
            var endDate = (DateTime)property.GetValue(validationContext.ObjectInstance);

            if (startDate == DateTime.MinValue || endDate == DateTime.MaxValue)
                return new ValidationResult("The start and end dates must be valid.");

            if (startDate >= endDate)
                return new ValidationResult("The start date cannot be greater than the end date.");

            return ValidationResult.Success;
        }
    }
}