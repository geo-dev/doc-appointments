namespace Appointments.CORE.Helpers
{
    public class AppointmentDateObject
    {
        private int _minutesToAdd;
        public DateTime StartDate { get; set; }
        public DateTime EndDate => StartDate.AddMinutes(_minutesToAdd);

        public AppointmentDateObject(int minutesToAdd, DateTime startDate)
        {
            _minutesToAdd = minutesToAdd;
            StartDate = startDate;
        }
    }
}