namespace Appointments.CORE.Extensions
{
    public static class StringExtensions
    {
        public static string ToBase64String(this string source)
        {
            var base64String = System.Text.Encoding.UTF8.GetBytes(source);
            return Convert.ToBase64String(base64String);
        }
    }
}