namespace Appointments.CORE.Extensions
{
    public static class DateExtensions
    {
        public static DateTime SetTime(this DateTime date, int hours, int minutes = 0, int seconds = 0)
        {
            return new DateTime(date.Year, date.Month, date.Day, hours, minutes, seconds);
        }

        public static DateTime FirstDayOfWeek(this DateTime date)
        {
            var culture = System.Globalization.CultureInfo.InvariantCulture;
            var diff = date.DayOfWeek - (culture.DateTimeFormat.FirstDayOfWeek + 1);

            if (diff < 0)
                diff += 7;

            return date.AddDays(-diff).Date;
        }

        public static DateTime FirstDayOfNextWeek(this DateTime date)
        {
            return date.AddDays(7).FirstDayOfWeek();
        }

        public static DateTime FirstDayOfPreviousWeek(this DateTime date)
        {
            return date.AddDays(-7).FirstDayOfWeek();
        }
    }
}