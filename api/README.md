# DoctorAppointmentAPI

This project was implemented in .NET 8.

![Swagger](https://i.imgur.com/fzeEyhZ.png)

## Setting up the application

In the `Appointments.Api` folder, an `appsettings.Example.json` file is included with the following content:
```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "AppointmentsAPI": {
    "BaseUri": "https://draliatest.azurewebsites.net/api/availability",
    "User": "",
    "Password": ""
  }
}
```

Copy the `appsettings.Example.json` file and rename it to `appsettings.json`, and provide values for `User` and `Password`.


## Run

From the `Appointments.Api` folder run `dotnet run`. Or `dotnet watch` to run it with hot reload enabled.

The API can be accessed via the URL: http://localhost:5391.

Swagger can be accessed via http://localhost:5391/swagger.

## Build

Run `dotnet build` to build the project.

## Running unit tests

From the `Appointments.Tests` folder run `dotnet test` to execute the unit tests via xUnit.

## Endpoints

The endpoints available are:

- `GET /api/appointments/availability/{date}`
    - You must pass a valid date with the format `yyyy-MM-dd`.
    - The date must be a Monday.
    - It must not be from the past (older than current week).
    - It will return a JSON with the following properties:
    ```json
    {
        "facilityId": "string",
        "monday": {
            "date": "string",
            "slots": [
                {
                    "startDate": "date",
                    "endDate": "date"
                }
            ]
        },
        "tuesday": {
            "date": "string",
            "slots": [
                {
                    "startDate": "date",
                    "endDate": "date"
                }
            ]
        },
        "wednesday": {
            "date": "string",
            "slots": [
                {
                    "startDate": "date",
                    "endDate": "date"
                }
            ]
        },
        "thursday": {
            "date": "string",
            "slots": [
                {
                    "startDate": "date",
                    "endDate": "date"
                }
            ]
        },
        "friday": {
            "date": "string",
            "slots": [
                {
                    "startDate": "date",
                    "endDate": "date"
                }
            ]
        }
    }
    ```
- `POST /api/appointments`
    - You must use the following request body:
    ```json
    {
        "facilityId": "string",
        "start": "date",
        "end": "date",
        "comments": "string?",
        "patient": {
            "name": "string",
            "secondName": "string?",
            "email": "string",
            "phone": "string"
        }
    }
    ```
    - The `facilityId`, `start`, `end`, `patient.name`, `patient.email` and `patient.phone` are required.
    - The start and end dates must be valid.
    - The email address must have a valid format.
    - The phone can only contain digits and spaces.
