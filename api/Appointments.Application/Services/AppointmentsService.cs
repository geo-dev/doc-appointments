using Appointments.CORE.DTOs;
using Appointments.CORE.Extensions;
using Appointments.CORE.Helpers;
using Appointments.CORE.Interfaces;
using Appointments.CORE.Models;

namespace Appointments.Application.Services
{
    public class AppointmentsService : IAppointmentsService
    {
        private readonly ISlotService _slotService;

        public AppointmentsService(ISlotService slotService)
        {
            _slotService = slotService;
        }

        public async Task<AvailableSlotsDTO?> GetWeeklySlotsAsync(DateTime week)
        {
            var result = await _slotService.GetWeeklySlotsAsync($"{week:yyyyMMdd}");
            if (result == null)
                return null;

            return GetAvailableSlotsDTO(week, result);
        }

        public async Task<bool> BookAppointmentAsync(AppointmentDTO appointment)
        {
            return await _slotService.BookAppointmentAsync(appointment);
        }


        private AvailableSlotsDTO GetAvailableSlotsDTO(DateTime startWeek, WeeklySlot weeklySlot)
        {
            return new AvailableSlotsDTO()
            {
                FacilityId = weeklySlot.Facility.FacilityId,
                Monday = GetDayDTO(startWeek, weeklySlot.SlotDurationMinutes, weeklySlot.Monday),
                Tuesday = GetDayDTO(startWeek.AddDays(1), weeklySlot.SlotDurationMinutes, weeklySlot.Tuesday),
                Wednesday = GetDayDTO(startWeek.AddDays(2), weeklySlot.SlotDurationMinutes, weeklySlot.Wednesday),
                Thursday = GetDayDTO(startWeek.AddDays(3), weeklySlot.SlotDurationMinutes, weeklySlot.Thursday),
                Friday = GetDayDTO(startWeek.AddDays(4), weeklySlot.SlotDurationMinutes, weeklySlot.Friday),
            };
        }

        private DayDTO GetDayDTO(DateTime startDate, int slotDuration, Day? day)
        {
            var times = new List<SlotDTO>();
            var response = new DayDTO()
            {
                Date = startDate.ToString("yyyy-MM-dd")
            };

            if (day != null && day.WorkPeriod != null && slotDuration > 0)
            {
                DateTime today = DateTime.Now;
                var currentDate = new AppointmentDateObject(slotDuration, startDate.SetTime(day.WorkPeriod.StartHour));
                DateTime workEndDate = startDate.SetTime(day.WorkPeriod.EndHour);
                DateTime lunchStart = startDate.SetTime(day.WorkPeriod.LunchStartHour);
                DateTime lunchEnd = startDate.SetTime(day.WorkPeriod.LunchEndHour);
                int lunchBreakDuration = day.WorkPeriod.LunchEndHour - day.WorkPeriod.LunchStartHour;

                while (currentDate.StartDate.Date >= today.Date && currentDate.EndDate <= workEndDate)
                {
                    if (currentDate.StartDate >= lunchStart && currentDate.StartDate <= lunchEnd)
                        currentDate.StartDate = currentDate.StartDate.AddHours(lunchBreakDuration);

                    if (IsSlotAvailable(currentDate, today, day.BusySlots))
                    {
                        times.Add(new SlotDTO()
                        {
                            StartDate = currentDate.StartDate,
                            EndDate = currentDate.EndDate
                        });
                    }

                    currentDate.StartDate = currentDate.StartDate.AddMinutes(slotDuration);
                }
            }

            response.Slots = times;
            return response;
        }

        private bool IsSlotAvailable(AppointmentDateObject date, DateTime today, List<BusySlot> busySlots)
        {
            return date.StartDate > today // The start date must be greater than today and
                && (busySlots == null || busySlots.Count == 0 // busySlots is empty or
                    || !busySlots.Exists(slot => // the date range does not overlap with any of the appointments time ranges
                        (date.StartDate >= slot.Start && date.StartDate < slot.End) // StartDate overlaps
                        || (date.EndDate > slot.Start && date.EndDate <= slot.End))); // EndDate overlaps
        }
    }
}