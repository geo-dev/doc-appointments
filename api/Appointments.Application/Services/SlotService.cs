using Appointments.CORE.Interfaces;
using Appointments.CORE.Extensions;
using Appointments.CORE.Models;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Appointments.CORE.DTOs;
using Microsoft.Extensions.Logging;

namespace Appointments.Application.Services
{    
    public class SlotService : ISlotService
    {
        private readonly RestClient _client;
        private readonly IConfiguration _configuration;
        private readonly ILogger<SlotService> _logger;
        private string _key;

        private readonly Dictionary<string, string?> _config;

        public SlotService(IConfiguration configuration, ILogger<SlotService> logger)
        {
            _client = new RestClient();
            _configuration = configuration;
            _logger = logger;

            _config = _configuration.GetSection("AppointmentsAPI").GetChildren().ToDictionary(x => x.Key, x => x.Value);
            _key = $"{_config["User"]}:{_config["Password"]}".ToBase64String();
        }

        public async Task<WeeklySlot?> GetWeeklySlotsAsync(string week)
        {
            try
            {
                var request = new RestRequest($"{_config["BaseUri"]}/GetWeeklyAvailability/{week}", Method.Get);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("Authorization", $"Basic {_key}");

                return await _client.GetAsync<WeeklySlot>(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> BookAppointmentAsync(AppointmentDTO appointment)
        {
            RestResponse? response;
            try
            {
                var request = new RestRequest($"{_config["BaseUri"]}/TakeSlot", Method.Post);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("Authorization", $"Basic {_key}");
                request.AddJsonBody(appointment);

                response = await _client.ExecuteAsync(request);
                if (!response.IsSuccessStatusCode && !string.IsNullOrWhiteSpace(response.Content))
                    _logger.LogError(response.Content, "Error while booking the appointment");

                return response.IsSuccessStatusCode;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}