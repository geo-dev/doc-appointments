﻿using Appointments.Application.Services;
using Appointments.CORE.DTOs;
using Appointments.CORE.Extensions;
using Appointments.CORE.Interfaces;
using Appointments.CORE.Models;
using Moq;
using Xunit;

namespace Appointments.Tests;

public class Test
{
    private readonly MockRepository _mockRepository;
    private readonly Mock<ISlotService> _mockSlotService;
    private DateTime _date;
    
    public Test()
    {
        _mockRepository = new MockRepository(MockBehavior.Strict);
        _mockSlotService = _mockRepository.Create<ISlotService>();
        _date = DateTime.Now;
    }

    [Fact]
    public async Task If_PreviousWeek_Then_ReturnAllEmpty()
    {
        // Arrange
        _date = _date.FirstDayOfPreviousWeek().AddDays(1);
        _mockSlotService.Setup(t => t.GetWeeklySlotsAsync(It.IsAny<string>())).Returns(Task.FromResult(GetMockData()));
        
        // Act
        var service = CreateService();
        var result = await service.GetWeeklySlotsAsync(_date.FirstDayOfWeek());

        // Assert
        Assert.NotNull(result);
        Assert.True(IsNullOrEmpty(result.Monday, result.Tuesday, result.Wednesday, result.Thursday, result.Friday));
        _mockRepository.VerifyAll();
    }

    [Fact]
    public async Task If_WeekIsGreaterOrEqualToThisWeek_Then_ReturnValidAvailableDates()
    {
        // Arrange
        var mockData = GetMockData();
        _date = _date.FirstDayOfNextWeek();
        _mockSlotService.Setup(t => t.GetWeeklySlotsAsync(It.IsAny<string>())).Returns(Task.FromResult(mockData));
        
        // Act
        var service = CreateService();
        var result = await service.GetWeeklySlotsAsync(_date.FirstDayOfWeek());

        // Assert
        Assert.NotNull(result);
        Assert.NotNull(result.Monday);
        Assert.True(AvailableSlotsAreGreaterThanNow(_date, result));
        Assert.True(result.Monday.Slots.All(slot => NotInBusySlots(slot, mockData!.Monday!.BusySlots)));
        Assert.True(result?.Monday?.Slots.Count > 0);
        Assert.False(LunchHourIsAvailable(mockData!.Monday!.WorkPeriod!, result.Monday.Slots));
        Assert.False(IsNullOrEmpty(result.Monday, result.Tuesday, result.Wednesday, result.Thursday, result.Friday));
        _mockRepository.VerifyAll();
    }

    [Fact]
    public async Task If_NoLunchHour_Then_CheckAllValidTimesAreAvailable()
    {
        // Arrange
        var mockData = GetMockData();
        mockData!.Friday!.WorkPeriod!.LunchStartHour = 0;
        mockData.Friday.WorkPeriod.LunchEndHour = 0;
        _date = _date.FirstDayOfNextWeek();
        _mockSlotService.Setup(t => t.GetWeeklySlotsAsync(It.IsAny<string>())).Returns(Task.FromResult((WeeklySlot?)mockData));
        
        // Act
        var service = CreateService();
        var result = await service.GetWeeklySlotsAsync(_date.FirstDayOfWeek());

        // Assert    
        Assert.NotNull(result);
        Assert.NotNull(result.Friday);
        Assert.True(AllTimesExceptBusySlotAreAvailable(_date, mockData.Friday,
            mockData.SlotDurationMinutes, result.Friday.Slots));
        _mockRepository.VerifyAll();
    }

    private AppointmentsService CreateService()
    {
        return new AppointmentsService(_mockSlotService.Object);
    }

    private WeeklySlot? GetMockData()
    {
        return new WeeklySlot()
        {
            Facility = new Facility(),
            SlotDurationMinutes = 20,
            Monday = new Day()
            {
                WorkPeriod = new WorkPeriod()
                {
                    StartHour = 9,
                    EndHour = 18,
                    LunchStartHour = 15,
                    LunchEndHour = 17
                },
                BusySlots = new()
                {
                    new BusySlot()
                    {
                        Start = _date.SetTime(9, 0),
                        End = _date.SetTime(9, 20)
                    },
                    new BusySlot()
                    {
                        Start = _date.SetTime(9, 20),
                        End = _date.SetTime(9, 40)
                    },
                    new BusySlot()
                    {
                        Start = _date.SetTime(10, 20),
                        End = _date.SetTime(10, 40)
                    },
                    new BusySlot()
                    {
                        Start = _date.SetTime(12, 40),
                        End = _date.SetTime(13, 00)
                    },
                    new BusySlot()
                    {
                        Start = _date.SetTime(17, 40),
                        End = _date.SetTime(18, 00)
                    }
                }
            },
            Wednesday = new Day()
            {
                WorkPeriod = new WorkPeriod()
                {
                    StartHour = 8,
                    EndHour = 18,
                    LunchStartHour = 14,
                    LunchEndHour = 15
                },
                BusySlots = new()
                {
                    new BusySlot()
                    {
                        Start = _date.AddDays(2).SetTime(08, 20),
                        End = _date.AddDays(2).SetTime(08, 40)
                    },
                    new BusySlot()
                    {
                        Start = _date.AddDays(2).SetTime(10, 00),
                        End = _date.AddDays(2).SetTime(10, 20)
                    },
                    new BusySlot()
                    {
                        Start = _date.SetTime(10, 20),
                        End = _date.SetTime(10, 40)
                    },
                    new BusySlot()
                    {
                        Start = _date.AddDays(2).SetTime(12, 40),
                        End = _date.AddDays(2).SetTime(13, 00)
                    }
                }
            },
            Friday = new Day()
            {
                WorkPeriod = new WorkPeriod()
                {
                    StartHour = 8,
                    EndHour = 15,
                    LunchStartHour = 13,
                    LunchEndHour = 14
                },
                BusySlots = new()
                {
                    new BusySlot()
                    {
                        Start = _date.AddDays(4).SetTime(08, 20),
                        End = _date.AddDays(4).SetTime(08, 40)
                    },
                    new BusySlot()
                    {
                        Start = _date.AddDays(4).SetTime(10, 00),
                        End = _date.AddDays(4).SetTime(10, 20)
                    },
                    new BusySlot()
                    {
                        Start = _date.AddDays(4).SetTime(10, 40),
                        End = _date.AddDays(4).SetTime(10, 50)
                    },
                    new BusySlot()
                    {
                        Start = _date.AddDays(4).SetTime(12, 00),
                        End = _date.AddDays(4).SetTime(12, 20)
                    }
                }
            }
        };
    }

    private WeeklySlot? GetMockDataWithVariableSlotDuration()
    {
        return new WeeklySlot()
        {
            Facility = new Facility(),
            SlotDurationMinutes = 15,
            Friday = new Day()
            {
                WorkPeriod = new WorkPeriod()
                {
                    StartHour = 8,
                    EndHour = 18,
                    LunchStartHour = 14,
                    LunchEndHour = 15
                },
                BusySlots = new()
                {
                    new BusySlot()
                    {
                        Start = _date.SetTime(08, 20),
                        End = _date.SetTime(08, 50)
                    },
                    new BusySlot()
                    {
                        Start = _date.SetTime(10, 10),
                        End = _date.SetTime(10, 25)
                    },
                    new BusySlot()
                    {
                        Start = _date.SetTime(10, 25),
                        End = _date.SetTime(10, 40)
                    },
                    new BusySlot()
                    {
                        Start = _date.SetTime(12, 40),
                        End = _date.SetTime(12, 55)
                    }
                }
            }
        };
    }

    private bool AvailableSlotsAreGreaterThanNow(DateTime now, AvailableSlotsDTO availableSlotsDto)
    {
        var day = now.Date.DayOfWeek;
        switch (day)
        {
            case DayOfWeek.Monday:
                return TimesAreValid(now, availableSlotsDto.Monday);
            case DayOfWeek.Tuesday:
                return IsNullOrEmpty(availableSlotsDto.Monday) && TimesAreValid(now, availableSlotsDto.Tuesday);
            case DayOfWeek.Wednesday:
                return IsNullOrEmpty(availableSlotsDto.Monday, availableSlotsDto.Tuesday)
                    && TimesAreValid(now, availableSlotsDto.Wednesday);
            case DayOfWeek.Thursday:
                return IsNullOrEmpty(availableSlotsDto.Monday, availableSlotsDto.Tuesday, 
                    availableSlotsDto.Wednesday) && TimesAreValid(now, availableSlotsDto.Thursday);
            case DayOfWeek.Friday:
                return IsNullOrEmpty(availableSlotsDto.Monday, availableSlotsDto.Tuesday,
                    availableSlotsDto.Wednesday, availableSlotsDto.Thursday) 
                    && TimesAreValid(now, availableSlotsDto.Friday);
            default:
                return IsNullOrEmpty(availableSlotsDto.Monday, availableSlotsDto.Tuesday,
                    availableSlotsDto.Wednesday, availableSlotsDto.Thursday, availableSlotsDto.Friday); 
        }
    }

    private bool TimesAreValid(DateTime now, DayDTO? dayDto)
    {
        if (dayDto == null)
            return true;

        var date = Convert.ToDateTime(now.Date);
        return dayDto.Slots.All(t => { return t.StartDate > now; });     
    }

    private bool NotInBusySlots(SlotDTO slot, List<BusySlot> busySlots)
    {
        return !busySlots.Exists(busy => (slot.StartDate >= busy.Start && slot.StartDate < busy.End)
                || (slot.EndDate > busy.Start && slot.EndDate <= busy.End));
    }

    private bool AllTimesExceptBusySlotAreAvailable(DateTime now, Day day, int slotDuration, List<SlotDTO> slots)
    {
        var currentDate = now.SetTime(day.WorkPeriod!.StartHour, 0);
        var endDate = now.SetTime(day.WorkPeriod!.EndHour, 0);
        var availableSlots = new List<SlotDTO>();
        var currentSlot = new SlotDTO();

        while (currentDate < endDate)
        {
            currentSlot.StartDate = currentDate;
            currentSlot.EndDate = currentDate.AddMinutes(slotDuration);
            if (NotInBusySlots(currentSlot, day.BusySlots))
                availableSlots.Add(currentSlot);

            currentDate = currentDate.AddMinutes(slotDuration);
        }

        return availableSlots.Count == slots.Count;
    }

    private bool LunchHourIsAvailable(WorkPeriod workPeriod, List<SlotDTO> slots) 
    {
        return slots.Exists(slot => {
            var lunchStartHour = slot.StartDate.SetTime(workPeriod.LunchStartHour, 0);
            var lunchEndHour = slot.StartDate.SetTime(workPeriod!.LunchEndHour, 0);
            return slot.StartDate >= lunchStartHour && slot.StartDate < lunchEndHour;
        });
    }

    private bool IsNullOrEmpty(params DayDTO?[] days)
    {
        return days == null || days.Length == 0 || days.All(d => d?.Slots.Count == 0);
    }
}
