using System.Reflection;
using Appointments.Application.Services;
using Appointments.CORE.Interfaces;

var builder = WebApplication.CreateBuilder(args);

builder.Logging.ClearProviders();
builder.Logging.AddConsole();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
  c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, 
  $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
});

var configuration = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json")
    .Build();

builder.Services.AddScoped<IAppointmentsService, AppointmentsService>();
builder.Services.AddSingleton<ISlotService, SlotService>();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.UseCors(x => x
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());

// app.UseHttpsRedirection();
app.MapControllers();

app.Run();
