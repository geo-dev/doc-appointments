using Appointments.CORE.DTOs;
using Appointments.CORE.Interfaces;
using Appointments.CORE.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;

namespace Appointments.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppointmentsController : Controller
    {
        private readonly IAppointmentsService _appointmentsService;
        private readonly ILogger<AppointmentsController> _logger;

        public AppointmentsController(IAppointmentsService appointmentsService, ILogger<AppointmentsController> logger)
        {
            _appointmentsService = appointmentsService;
            _logger = logger;
        }

        /// <summary>
        /// It returns the available slots for the week, considering the doctor's working hours and existing bookings
        /// </summary>
        /// <param name="date">The start of the week to check. It must be a Monday</param>
        /// <returns>Returns an object containing the available slots for each day of the week</returns>
        [HttpGet("availability/{date}")]
        public async Task<IActionResult> GetAvailability(DateTime date)
        {
            try 
            {
                DateTime firstDayOfWeek = DateTime.Now.FirstDayOfWeek();
                if (date < firstDayOfWeek)
                    return BadRequest("The date must be from this week onwards.");                

                if (date.Date.DayOfWeek != DayOfWeek.Monday)
                    return BadRequest("The date must be a Monday.");

                var response = await _appointmentsService.GetWeeklySlotsAsync(date);
                return response != null ? Ok(response) : NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{controller},{method} Error while fetching the data", GetType().Name, MethodBase.GetCurrentMethod().Name);
                return BadRequest("There was an error while fetching the data.");
            }
        }

        /// <summary>
        /// Books an appointment for a patient
        /// </summary>
        /// <param name="request">The details of the appointment to be booked, including the facility id, 
        /// patient information, and desired time slot</param>
        /// <returns>Returns NoContent if the booking is successful, BadRequest otherwise</returns>
        [HttpPost]
        public async Task<IActionResult> BookAppointment([FromBody] AppointmentDTO request)
        {
            try 
            {
                var isSuccessful = await _appointmentsService.BookAppointmentAsync(request);
                if (isSuccessful)
                    return NoContent();

                return BadRequest("There was an error while booking the appointment.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "{controller},{method} Error while booking the appointment", GetType().Name, MethodBase.GetCurrentMethod().Name);
                return BadRequest("There was an error while booking the appointment.");
            }
        }
   }
}