import { Component } from '@angular/core';
import { ApiService as ApiService } from '../../core/services/api.service';
import { AvailableSlotsDTO, SlotDTO } from '../../core/models/slot.model';
import {
  BehaviorSubject,
  Subject,
  finalize,
  take,
  takeUntil,
} from 'rxjs';
import { CommonModule } from '@angular/common';
import { dateIsGreaterOrEqualThanNow, dateToString, getCurrentWeekMonday } from '../../core/utils';
import { CalendarComponent } from '../../core/components/calendar/calendar.component';
import { BookingFormComponent } from '../../core/components/booking-form/booking-form.component';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from '../../shared/services/notification.service';
import { AppointmentDTO } from '../../core/models/appointment.model';
import moment from 'moment';

@Component({
  selector: 'app-home',
  standalone: true,
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
  imports: [CommonModule, CalendarComponent],
})
export class HomeComponent {
  private currentDate$ = new BehaviorSubject<Date>(getCurrentWeekMonday());
  private onDestroy$ = new Subject<boolean>();
  weeklySlots: AvailableSlotsDTO;
  startDate: Date;
  endDate: Date;
  isLoading: boolean;

  constructor(
    private apiService: ApiService,
    private notificationsService: NotificationService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.currentDate$.
      pipe(takeUntil(this.onDestroy$))
      .subscribe((date) => {
      if (date) {
        this.startDate = moment(date).weekday(1).toDate();
        this.endDate = moment(date).weekday(5).toDate();
        this.getAvailability(date);
      }
    });

    this.notificationsService
      .getSelectedSlot()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data) => {
        if (data) {
          this.openBookingForm(data);
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next(true);
    this.onDestroy$.complete();
  }

  nextWeek() {
    const newDate = moment(this.currentDate$.value)
      .add(1, 'weeks')
      .startOf('isoWeek');
    this.currentDate$.next(newDate.toDate());
  }

  previousWeek() {
    const newDate = moment(this.currentDate$.value)
      .subtract(1, 'weeks')
      .startOf('isoWeek');
    if (dateIsGreaterOrEqualThanNow(newDate)) {
      this.currentDate$.next(newDate.toDate());
    }
  }

  private getAvailability(week: Date) {
    this.isLoading = true;

    this.apiService
      .getAvailableSlots(dateToString(week))
      .pipe(
        takeUntil(this.onDestroy$),
        finalize(() => this.isLoading = false)
      )
      .subscribe({
        next: (data) => {
          this.weeklySlots = data;
        },
        error: () => {
          this.clearSlots();
        }
      });
  }

  private openBookingForm(slot: SlotDTO) {
    const dialogRef = this.dialog.open(BookingFormComponent, {
      data: {
        startDate: slot.startDate,
        endDate: slot.endDate,
      },
      disableClose: false,
      width: '500px',
      height: 'fit-content',
    });

    dialogRef.componentInstance.onAcceptEvent.subscribe((data) => {
      const appointment: AppointmentDTO = {
        facilityId: this.weeklySlots.facilityId,
        start: slot.startDate,
        end: slot.endDate,
        comments: data.comments,
        patient: {
          name: data.name,
          secondName: data.secondName,
          email: data.email,
          phone: data.phone,
        },
      };

      this.apiService
        .bookAppointment(appointment)
        .pipe(take(1))
        .subscribe(() => {
          this.getAvailability(this.currentDate$.value);
          this.notificationsService.setMessage(
            'Appointment booked successfully.'
          );
        });
      dialogRef.close();
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.notificationsService.setSelectedSlot(null);
      });
  }

  private clearSlots() {
    if (this.weeklySlots) {
      this.weeklySlots.monday.slots = [];
      this.weeklySlots.tuesday.slots = [];
      this.weeklySlots.wednesday.slots = [];
      this.weeklySlots.thursday.slots = [];
      this.weeklySlots.friday.slots = [];
    }
  }
}
