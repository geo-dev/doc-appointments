import { AbstractControl } from '@angular/forms';

export const phoneValidator = () => {
  return (control: AbstractControl): { [key: string]: any } | null => {
    // It can only contain digits or spaces
    const phoneRegex = /^[0-9\s]+$/;
    const phoneIsValid = phoneRegex.test(control.value);

    if (!phoneIsValid) {
      return { isNotValidPhone: true };
    }

    return null;
  };
};
