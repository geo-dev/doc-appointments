import { AbstractControl } from '@angular/forms';

export const emailValidator = () => {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const emailIsValid = emailRegex.test(control.value);

    if (!emailIsValid) {
      return { isNotValidEmail: true };
    }

    return null;
  };
};
