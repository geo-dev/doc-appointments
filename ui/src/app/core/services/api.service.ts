import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { AvailableSlotsDTO } from '../models/slot.model';
import { NotificationService } from '../../shared/services/notification.service';
import { AppointmentDTO } from '../models/appointment.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private _baseUri = environment.apiUri;

  constructor(private http: HttpClient, private notificationsService: NotificationService) { }

  getAvailableSlots(week: string): Observable<AvailableSlotsDTO> {
    return this.http.get<AvailableSlotsDTO>(`${this._baseUri}/appointments/availability/${week}`)
      .pipe(
        catchError(err => this.handleError(err, 'An error ocurred while getting the available slots.'))
      );
  }

  bookAppointment(appointment: AppointmentDTO) {
    return this.http.post<any>(`${this._baseUri}/appointments`, appointment)
      .pipe(
        catchError(err => this.handleError(err, 'An error ocurred while booking the appointment.'))
      );
  }

  private handleError(error: HttpErrorResponse, message: string) {
    if (error.status === 0) {
      console.error('An error occurred:', error.error);
    } else {
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }

    this.notificationsService.setMessage(message, true);

    return throwError(() => new Error(message));
  }
}
