import moment, { Moment } from "moment";

export const dateToString = (date: Date): string => {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
}

export const getCurrentWeekMonday = () => {
    const currentDate = new Date();
    const currentWeekMonday = moment(currentDate).weekday(1).toDate();
    const currentWeekFriday = moment(currentDate).weekday(5).toDate();
    if (currentDate > currentWeekFriday) {
        return moment(currentDate).add(1, 'weeks').startOf('isoWeek').toDate();
    }

    return currentWeekMonday;
}

export const dateIsGreaterOrEqualThanNow = (date: Moment): boolean => {
    return date.clone().startOf('day') >= moment(new Date()).startOf('day');
}