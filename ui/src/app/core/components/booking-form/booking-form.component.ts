import { Component, EventEmitter, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DatePipe } from "../../pipes/date.pipe";
import { emailValidator } from '../../validators/email.validator';
import { phoneValidator } from '../../validators/phone.validator';

@Component({
    selector: 'app-booking-form',
    standalone: true,
    templateUrl: './booking-form.component.html',
    styleUrl: './booking-form.component.scss',
    imports: [SharedModule, DatePipe]
})
export class BookingFormComponent {
  fb: FormBuilder = new FormBuilder();
  form: FormGroup;

  onAcceptEvent = new EventEmitter();
  get startDate() {
    return this.form.controls['startDate'];
  }

  get email() {
    return this.form.controls['email'];
  }

  get phone() {
    return this.form.controls['phone'];
  }

  constructor(public dialogRef: MatDialogRef<BookingFormComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit(): void {
    this.buildForm();

    if (this.data) {
      this.form.controls['startDate'].patchValue(this.data.startDate);
      this.form.controls['endDate'].patchValue(this.data.endDate);
    }
  }

  onAccept() {
    if (this.form.valid) {
      this.onAcceptEvent.emit(this.form.value)
    }
  }

  private buildForm() {
    this.form = this.fb.group({
      startDate: [''],
      endDate: [''],
      comments: [''],
      name: ['', Validators.required],
      secondName: [''],
      email: ['', [Validators.required, emailValidator()]],
      phone: ['', [Validators.required, phoneValidator()]]
    });
  }
}
