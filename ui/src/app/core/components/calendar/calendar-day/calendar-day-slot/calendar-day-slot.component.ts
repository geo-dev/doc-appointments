import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SlotDTO } from '../../../../models/slot.model';
import { TimePipe } from "../../../../pipes/time.pipe";
import { NotificationService } from '../../../../../shared/services/notification.service';

@Component({
    selector: 'app-calendar-day-slot',
    standalone: true,
    templateUrl: './calendar-day-slot.component.html',
    styleUrl: './calendar-day-slot.component.scss',
    imports: [TimePipe]
})
export class CalendarDaySlotComponent {
  @Input() slot: SlotDTO;

  constructor(private notificationsService: NotificationService) {}

  onSelectedSlot(value: SlotDTO) {
    this.notificationsService.setSelectedSlot(value);
  }
}
