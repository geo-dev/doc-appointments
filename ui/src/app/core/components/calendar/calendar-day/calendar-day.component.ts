import { Component, Input } from '@angular/core';
import { CalendarDaySlotComponent } from "./calendar-day-slot/calendar-day-slot.component";
import { DayDTO } from '../../../models/slot.model';

@Component({
    selector: 'app-calendar-day',
    standalone: true,
    templateUrl: './calendar-day.component.html',
    styleUrl: './calendar-day.component.scss',
    imports: [CalendarDaySlotComponent]
})
export class CalendarDayComponent {
  @Input() name: string;
  @Input() day: DayDTO | undefined;
}
