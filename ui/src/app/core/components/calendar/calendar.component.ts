import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AvailableSlotsDTO } from '../../models/slot.model';
import { CalendarDayComponent } from './calendar-day/calendar-day.component';
import { MatIconModule } from '@angular/material/icon';
import { DayPipe } from "../../pipes/day.pipe"; 
import moment from 'moment';
import { getCurrentWeekMonday } from '../../utils';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@Component({
    selector: 'app-calendar',
    standalone: true,
    templateUrl: './calendar.component.html',
    styleUrl: './calendar.component.scss',
    imports: [CalendarDayComponent, MatIconModule, MatProgressBarModule, DayPipe]
})
export class CalendarComponent {
  @Input() weeklySlots: AvailableSlotsDTO | null;
  @Input() startDate: Date;
  @Input() endDate: Date;
  @Input() isLoading: boolean;
  @Output() nextWeekEvent = new EventEmitter<void>();
  @Output() previousWeekEvent = new EventEmitter<void>();

  constructor() {}

  ngOnChanges(): void { }
  
  nextWeek() {
    this.nextWeekEvent.emit();
  }

  previousWeek() {
    this.previousWeekEvent.emit();
  }

  isCurrentWeek() {
    return moment(this.startDate).isSameOrBefore(getCurrentWeekMonday(), 'day');
  }

}
