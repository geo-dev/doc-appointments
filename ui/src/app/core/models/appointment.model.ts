import { PatientDTO } from "./patient.model";

export interface AppointmentDTO {
    facilityId: string,
    start: Date,
    end: Date,
    comments: string | null,
    patient: PatientDTO
}