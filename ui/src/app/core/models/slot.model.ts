export interface AvailableSlotsDTO {
    facilityId: string,
    monday: DayDTO,
    tuesday: DayDTO,
    wednesday: DayDTO,
    thursday: DayDTO,
    friday: DayDTO
}

export interface DayDTO {
    date: string,
    slots: SlotDTO[]
}

export interface SlotDTO {
    startDate: Date,
    endDate: Date
}