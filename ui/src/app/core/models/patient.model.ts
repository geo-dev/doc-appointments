export interface PatientDTO {
    name: string,
    secondName: string | null,
    email: string,
    phone: string
}