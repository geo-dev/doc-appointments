import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

@Pipe({
  name: 'date',
  standalone: true
})
  
export class DatePipe implements PipeTransform {

  transform(date: Date): string {
    return moment(date).format("DD/MM/yyyy HH:mm");
  }

}
