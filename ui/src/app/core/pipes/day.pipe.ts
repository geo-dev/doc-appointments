import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

@Pipe({
  name: 'day',
  standalone: true
})
export class DayPipe implements PipeTransform {

  transform(date: Date): string {
    const momentObj = moment(date);
    return `${momentObj.format('ddd')} ${momentObj.format('DD/MM/yyyy')}`;
  }

}
