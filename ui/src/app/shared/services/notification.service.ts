import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SlotDTO } from '../../core/models/slot.model';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private selectedSlot$ = new BehaviorSubject<SlotDTO | null>(null);
  private horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  private verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(private snackBar: MatSnackBar) { }

  setMessage(message: string | null, isError: boolean = false) {
    if (message) {
      this.openSnackBar(message, 4000, isError);
    }
  }

  setSelectedSlot(slot: SlotDTO | null) {
    this.selectedSlot$.next(slot);
  }

  getSelectedSlot() {
    return this.selectedSlot$.asObservable();
  }

  openSnackBar(message: string, durantion: number, isError: boolean) {
    let snackBarConfig: MatSnackBarConfig<any> = {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: durantion,
    }

    if (isError) {
      snackBarConfig = {...snackBarConfig, panelClass: ['danger-snackbar']
      }
    }

    this.snackBar.open(message, '', snackBarConfig);
  }

}
