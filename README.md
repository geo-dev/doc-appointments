# Table of Contents
[[_TOC_]]

## Scope / Task

### Backend Functionality
The backend code receives a date representing the Monday of a week that is on or after the current week, and retrieves the availability for that week based on this date.

More information at [api/README](api/README.md).

#### Date Validation:
- Ensure the provided date is a Monday.
- Confirm the date is not in the past.

#### Retrieve Availability:
- For each day of the week starting from the given Monday:
    - Analyze the doctor's working hours.
    - Consider lunch breaks.
    - Account for already booked slots.
- Return a list of available times for each day of the week.

#### Appointment Booking:
- Check the required data is present (dates, patient information).
- Check the data is in a valid format (dates, required fields, email validation).
- Books the appointment.

### Frontend Functionality

The frontend allows users to view and interact with the calendar, retrieve availability information, and book appointments.

More information at [ui/README](ui/README.md).

#### Calendar Visualization:
- Enable the user to navigate through different weeks on the calendar.

#### Retrieve Availability:
- Fetch the availability data from the backend API for the selected week.

#### Appointment Booking:
- Allow the user to fill in required information.
- Submit the booking request to the backend.


## Technical Aspects
- The backend is implemented in <b>.NET 8</b>.
- The backend has the following layers:
    - <b>Api</b>: The public side of the application, where the controller resides.
    - <b>Application</b>: It includes services for both handling business logic and interfacing with the external API.
    - <b>CORE</b>: It includes the interfaces, models and DTOs. To keep it more simple, I didn't create a separate layer for shared code, so it also contains the extensions and helper methods.
    - <b>Tests</b>: It includes the file with the unit tests. Since the DTO validations for the POST are done by the framework via validation attributes, I didn't write tests for that method.
- Unit tests are implemented using <b>xUnit</b> and <b>Moq</b>.
- The frontend is implemented in <b>Angular 18</b>.
- <b>Dockerfiles</b> for both projects are provided, as well as a <b>docker-compose</b> file for easier execution of the applications.


## Configuration Setup
### Appsettings <a id="appsettings" name="appsettings"></a>
To ensure credentials are not commited to the repository, the production `appsettings.json` has not been included, but an `appsettings.Example.json` has been provided instead:

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "AppointmentsAPI": {
    "BaseUri": "https://draliatest.azurewebsites.net/api/availability",
    "User": "",
    "Password": ""
  }
}
```
Copy the `appsettings.Example.json` file and rename it to `appsettings.json`, and provide values for `User` and `Password`.


## How To Run

### Running With Docker Compose

Make sure you have [Docker](https://docs.docker.com/engine/install) installed before proceeding.

Once the API credentials have been provided in the `appsettings.json` file (refer to [Appsettings](#appsettings)) you can execute the following commands, from the root directory of the project, which will serve both applications:

```sh
docker compose build # It creates the containers as defined by the Dockerfiles present
docker compose up -d # It runs the containers
``` 

After completing these steps, the app can be accessed at http://localhost:9080, the API at http://localhost:9080/api, and Swagger at http://localhost:9080/swagger.


## Development Environment Setup

### Backend

To run the .NET application you need to install:
- [.NET](https://learn.microsoft.com/en-us/dotnet/core/install) (version 8.0).

Once the API credentials have been provided in the `appsettings.json` file (refer to [Appsettings](#appsettings)), you can execute the following command inside the `api/Appointments.Api` folder:

```sh
dotnet run # It compiles, intalls the dependencies and executes the project
```

To run the tests, you can execute the following command from the `api/Appointments.Tests` folder:

```sh
dotnet test # It executes the tests found in the project and displays the results in the terminal
```

The API can be accessed via the URL: http://localhost:5391

The endpoints available are:

- `GET /api/appointments/availability/{date}`
- `POST /api/appointments`

(More details at [api/README](api/README.md))

![Swagger](https://i.imgur.com/fzeEyhZ.png)


### Frontend
To run the Angular application, you need to install (if they're not already installed):
- [Node.js](https://nodejs.org/en) (versions ^18.19.1 || ^20.11.1 || ^22.0.0).
- [Angular CLI](https://v17.angular.io/guide/setup-local) (version ^17.0.1). With <b>npm</b>: `npm install -g @angular/cli`.

Inside the root folder of the application (`ui/`), you must run the commands:

```sh
npm install # It installs the required dependencies for the project
ng serve # It launches the application
```

The app can be accessed via the URL: http://localhost:4200

![Angular](https://i.imgur.com/850GrBt.png)